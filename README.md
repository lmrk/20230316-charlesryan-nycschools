# 20230316-charlesryan-NYCSchools



## Description
Completed coding assignment for Virtusa. This application shows a list of schools in NYC and when the user selects a school, they are taken to a new activity that displays the School's SAT Scores.

Data is gathered from server.

## Technical Details
- **Coding Language:** Kotlin
- **Architecture:** MVVM with repository pattern
- **Http Client:** Retrofit
- **Dependency Injection:** Dagger Hilt
- **Asynchronous Programming:** Coroutines
- **Other:** ViewBinding
