package com.charles.virtusa.nycschools.model

import com.google.gson.annotations.SerializedName

data class SATScore(
    @SerializedName("dbn")
    val dbn: String,

    @SerializedName("num_of_sat_test_takers")
    val numOftestTakers: String,

    @SerializedName("sat_critical_reading_avg_score")
    val criticalReadingAvg: String,

    @SerializedName("sat_math_avg_score")
    val mathAvg: String,

    @SerializedName("sat_writing_avg_score")
    val writingAvg: String
)