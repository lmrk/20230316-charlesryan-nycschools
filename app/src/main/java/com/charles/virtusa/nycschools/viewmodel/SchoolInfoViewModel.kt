package com.charles.virtusa.nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.charles.virtusa.nycschools.model.Repository
import com.charles.virtusa.nycschools.model.SATScore
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolInfoViewModel @Inject constructor(val repository: Repository) : ViewModel() {

    private val _satScore = MutableLiveData<List<SATScore>>()
    val satScore: LiveData<List<SATScore>> = _satScore

    private val _message = MutableLiveData<String>()
    val message: LiveData<String> = _message

    private val _processing = MutableLiveData<Boolean>()
    val processing: LiveData<Boolean> = _processing

    fun getSATData(dbn: String) {

        viewModelScope.launch(IO) {
            try {


                _processing.postValue(true)

                val response = repository.getSatScore(dbn)

                if (!response.isSuccessful) {
                    _message.postValue("Failed to load data. Please retry.")
                    return@launch
                }

                val satScoreList = response.body()

                if (satScoreList == null) {
                    _message.postValue("Empty response from server")
                    return@launch
                }

                _satScore.postValue(satScoreList)
            } catch (e: Exception) {
                _processing.postValue(false)
                _message.postValue("Error is: $e")
            }
        }
    }
}