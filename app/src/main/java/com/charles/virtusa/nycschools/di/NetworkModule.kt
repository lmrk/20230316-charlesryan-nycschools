package com.charles.virtusa.nycschools.di

import com.charles.virtusa.nycschools.common.Constants
import com.charles.virtusa.nycschools.model.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Provides
    fun providesApiService(retrofit: Retrofit) = retrofit.create(ApiService::class.java)

    @Provides
    fun providesRetrofit(client: OkHttpClient) = Retrofit.Builder().apply {
        baseUrl(Constants.BASE_URL)
        addConverterFactory(GsonConverterFactory.create())
        client(client)
    }.build()

    @Provides
    fun providesClient(): OkHttpClient {
        val interceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
        val client = OkHttpClient.Builder().apply {
            addInterceptor(interceptor)
        }.build()
        return client
    }

}