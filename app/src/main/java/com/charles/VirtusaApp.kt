package com.charles

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class VirtusaApp: Application() {
}